import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { AccidentService } from './accident.service';
import { CreateAccidentDto } from './dtos/create-accident.dto';

@Controller('accidents')
export class AccidentController {
  constructor(private readonly accidentService: AccidentService) {}

  @Get()
  async getAllAccidents() {
    const accidents = await this.accidentService.getAllAccidents();

    return accidents;
  }

  @Get('/active')
  async getActiveAccidents() {
    const accidents = await this.accidentService.getAccidentsTaken();

    return accidents;
  }

  @Get('/finished')
  async getAccidentsFinished() {
    const accidents = await this.accidentService.getAccidentsFinished();

    return accidents;
  }

  @Get('/no-active')
  async getNoActiveAccidents() {
    const accidents = await this.accidentService.getNoActiveAccidents();

    return accidents;
  }

  @Post()
  async createAccident(@Body() dto: CreateAccidentDto) {
    const accidentCreated = await this.accidentService.createAccident(dto);

    return accidentCreated;
  }

  @Delete(':id')
  async deleteAccident(@Param('id') id: string) {
    const accidentDeleted = await this.accidentService.deleteAccident(id);

    return accidentDeleted;
  }

  @Put(':id')
  async updateAccident(@Param('id') id: string, @Body() dto: any) {
    const accident = await this.accidentService.editAccident(id, dto);

    return accident;
  }

  @Get(':id')
  async getAccidentById(@Param('id') id: string) {
    const accident = await this.accidentService.getOne(id);

    return accident;
  }

  @Get('user/:id')
  async getAccidentsByUserId(@Param('id') id: string) {
    const accidents = await this.accidentService.getAccidentsByUserId(id);

    return accidents;
  }

  @Get('police/:id')
  async getAccidentsByPoliceId(@Param('id') id: string) {
    const accidents = await this.accidentService.getAccidentsByPoliceId(id);

    return accidents;
  }
}
