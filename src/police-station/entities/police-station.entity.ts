import { User } from 'src/user/entities';
import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity('police_stations')
export class PoliceStation {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'name', type: 'varchar' })
  name: string;

  @Column({ name: 'latitude', type: 'varchar' })
  latitude: string;

  @Column({ name: 'longitude', type: 'varchar' })
  longitude: string;

  @Column({ name: 'address', type: 'varchar' })
  address: string;

  @Column({ name: 'district', type: 'varchar' })
  district: string;

  @OneToMany((type) => User, (user) => user.policeStation, {
    onDelete: 'CASCADE',
  })
  users: User[];
}
