import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Accident } from './entities/accident.entity';
import { CreateAccidentDto } from './dtos/create-accident.dto';
import { User } from 'src/user/entities';
import { UserService } from '../user/user.service';

@Injectable()
export class AccidentService {
  constructor(
    @InjectRepository(Accident)
    private readonly accidentRepository: Repository<Accident>,
    private readonly userRepository: UserService,
  ) {}

  async getAllAccidents() {
    const accidents = await this.accidentRepository.find({
      relations: ['user'],
    });

    return accidents;
  }

  async getAccidentsTaken() {
    const accidents = await this.accidentRepository.find({
      where: [{status:1}, {status: 2}],
      relations: ['user'],
    });

    return accidents;
  }

  async getAccidentsFinished() {
    const accidents = await this.accidentRepository.find({
      where: { status: 2 },
      relations: ['user'],
    });

    return accidents;
  }

  async getNoActiveAccidents() {
    const accidents = await this.accidentRepository.find({
      where: { status: 0 },
      relations: ['user'],
    });

    return accidents;
  }

  async createAccident(dto: CreateAccidentDto) {
    const user = await this.userRepository.findOne({ id: dto.user });
    if (!user) throw new NotFoundException('User does not exists');
    const accidentToAdd = {
      ...dto,
      user,
    };

    const accident = await this.accidentRepository.create(accidentToAdd);

    return await this.accidentRepository.save(accident);
  }

  async deleteAccident(id: string) {
    const deleted = await this.accidentRepository.delete(id);

    return deleted;
  }

  async editAccident(id: string, dto: any) {
    console.log('ESTO SE ENVIA PARA EDITARRR', id, dto);
    const accident = await this.accidentRepository.findOne(id);
    if (!accident) throw new NotFoundException('Accident does not exist');
    let police = null;
    if (dto?.userPolice > 0) {
      police = await this.userRepository.findOne({
        id: dto?.userPolice,
      });
      if (police.userType !== 'POLICE')
        throw new NotFoundException(
          'Solo el policia puede escoger un accidente',
        );
    }
    const accidentEdited = Object.assign(accident, dto);
    const resp = await this.accidentRepository.save(accidentEdited);
    return {
      ...resp,
      userPolice: police,
    };
    // return await this.accidentRepository.save(accidentEdited);
  }

  async getOne(id: string) {
    const accident = await this.accidentRepository.findOne({
      where: { id },
      relations: ['user'],
    });
    console.log(accident);

    if (!accident) throw new NotFoundException('Accident not found');

    return accident;
  }

  async getAccidentsByUserId(id: string) {
    const accidents = await this.accidentRepository.find({
      where: { user: id },
      relations: ['user'],
    });

    if (!accidents) throw new NotFoundException('Accidents not found');

    return accidents;
  }

  async getAccidentsByPoliceId(id: string) {
    const accidents = await this.accidentRepository.find({
      where: { userPolice: id },
      relations: ['user'],
    });

    if (!accidents) throw new NotFoundException('Accidents not found');

    return accidents;
  }
}
