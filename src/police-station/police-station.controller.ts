import { Controller, Get } from '@nestjs/common';
import { PoliceStationService } from './police-station.service';

@Controller('police-stations')
export class PoliceStationController {
  constructor(private readonly policeStationService: PoliceStationService) {}

  @Get()
  async getAllStations() {
    const stations = await this.policeStationService.getAllStations();

    return stations;
  }
}
