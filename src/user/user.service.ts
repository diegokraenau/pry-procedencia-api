import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dtos/create-user.dto';
import { LoginDto } from './dtos/login.dto';
import { JwtService } from '@nestjs/jwt';
import { PoliceStationService } from '../police-station/police-station.service';

export interface UserFindOne {
  id?: string;
  email?: string;
  password?: string;
}

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
    private readonly policeStationService: PoliceStationService,
  ) {}

  async getAllUsers() {
    const users = await this.userRepository.find();

    return users;
  }

  async getAllPoliceUsers() {
    const users = await this.userRepository.find();
    const polices = [];
    for (let i = 0; i < users.length; i++) {
      if (users[i].userType === 'POLICE') {
        polices.push(users[i]);
      }
    }
    return polices;
  }

  async createUser(dto: CreateUserDto) {
    const userToCreate = {
      ...dto,
      policeStation: dto.policeStation
        ? await this.policeStationService.getOne(dto.policeStation)
        : null,
    };
    const user = await this.userRepository.create(userToCreate);

    return await this.userRepository.save(user);
  }

  async findOne(data: UserFindOne) {
    return await this.userRepository
      .createQueryBuilder('user')
      .where(data)
      .addSelect('user.password')
      .getOne();
  }

  async getOne(id: string, useEntity?: User) {
    const user = await this.userRepository
      .findOne(id)
      .then((u) => (!useEntity ? u : !!u && useEntity.id === u.id ? u : null));

    if (!user)
      throw new NotFoundException('User does not exists or unauthorized');

    return user;
  }

  async login(dto: LoginDto) {
    const user = await this.findOne({
      email: dto.email,
      password: dto.password,
    });

    if (!user)
      throw new NotFoundException('User does not exists or unauthorized');

    return {
      token: this.jwtService.sign({
        id: user.id,
        name: user.name,
        email: user.email,
        role: user.userType,
      }),
    };
  }

  async deleteUser(id: string) {
    const deleted = await this.userRepository.delete(id);

    return deleted;
  }

  async editUser(id: string, dto: any) {
    const user = await this.userRepository.findOne(id);
    if (!user) throw new NotFoundException('User does not exist');

    await this.validateNotDuplicateEmail(
      dto.email,
      dto.dni,
      dto.phone,
      dto.license,
      dto.userType === 'POLICE' ? true : false,
      user.id,
    );

    const userEdited = Object.assign(user, dto);
    return await this.userRepository.save(userEdited);
  }

  async validateNotDuplicateEmail(
    email: string,
    dni: string,
    phone: string,
    license: string,
    isPolice: boolean,
    user?: string,
  ) {
    const errorArr: any[] = [];
    const emailDuplicate = await this.userRepository.findOne({
      email,
    });

    if (emailDuplicate)
      errorArr.push({
        element: 'email',
        duplicate: true,
        user: emailDuplicate.id,
      });

    const dniDuplicate = await this.userRepository.findOne({
      dni,
    });

    if (dniDuplicate)
      errorArr.push({
        element: 'dni',
        duplicate: true,
        user: dniDuplicate.id,
      });

    const phoneDuplicate = await this.userRepository.findOne({
      phone,
    });

    if (phoneDuplicate)
      errorArr.push({
        element: 'phone',
        duplicate: true,
        user: phoneDuplicate.id,
      });

    let licenseDuplicate = null;
    if (isPolice) {
      licenseDuplicate = await this.userRepository.findOne({
        license,
      });
    }

    if (licenseDuplicate) {
      errorArr.push({
        element: 'license',
        duplicate: true,
        user: licenseDuplicate.id,
      });
    }

    if (
      user &&
      errorArr.length > 0 &&
      errorArr.filter((x) => x.user !== user).length > 0
    ) {
      this.showError(errorArr.filter((x) => x.user !== user));
    }

    if (!user && errorArr.length > 0) {
      this.showError(errorArr);
    }
  }

  private showError(duplicates: any[]) {
    throw new NotFoundException(duplicates);
  }
}
