import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PoliceStation } from './entities/police-station.entity';

@Injectable()
export class PoliceStationService {
  constructor(
    @InjectRepository(PoliceStation)
    private readonly policeStationRepository: Repository<PoliceStation>,
  ) {}

  async getOne(id: string, policeStationEntity?: PoliceStation) {
    const policeStation = await this.policeStationRepository
      .findOne(id)
      .then((u) =>
        !policeStationEntity
          ? u
          : !!u && policeStationEntity.id === u.id
          ? u
          : null,
      );

    if (!policeStation)
      throw new NotFoundException('Police station does not exists');

    return policeStation;
  }

  async getAllStations() {
    const stations = await this.policeStationRepository.find();

    return stations;
  }

}
