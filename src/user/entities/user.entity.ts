import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Accident } from '../../accident/entities/accident.entity';
import { PoliceStation } from '../../police-station/entities/police-station.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'dni', type: 'varchar', nullable: true })
  dni: string;

  @Column({ name: 'name', type: 'varchar', nullable: true })
  name: string;

  @Column({ name: 'license', type: 'varchar', nullable: true })
  license: string;

  @Column({ name: 'patrol_number', type: 'varchar', nullable: true })
  patrolNumber: string;

  @Column({ name: 'birth_date', type: 'varchar', nullable: true })
  dateOfBirth: string;

  @Column({
    name: 'user_type',
    type: 'varchar',
    default: 'POLICE',
    nullable: true,
  })
  userType: string;

  @Column({ name: 'password', type: 'varchar', nullable: true })
  password: string;

  @Column({ name: 'phone', type: 'varchar', nullable: true })
  phone: string;

  @Column({ name: 'email', type: 'varchar', nullable: true })
  email: string;

  @OneToMany((type) => Accident, (accident) => accident.user, {
    onDelete: 'CASCADE',
  })
  accidents: Accident[];

  @ManyToOne((type) => PoliceStation, (policeStation) => policeStation.users, {
    nullable: true,
  })
  @JoinColumn({ name: 'police_station_id' })
  policeStation: PoliceStation;
}
