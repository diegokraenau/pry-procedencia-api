import {
  Controller,
  Get,
  Body,
  Post,
  Param,
  Delete,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dtos/create-user.dto';
import { LoginDto } from './dtos/login.dto';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  async getAllUsers() {
    const users = await this.userService.getAllUsers();

    return users;
  }
  @Get('polices')
  async getAllPolices() {
    const polices = await this.userService.getAllPoliceUsers();

    return polices;
  }

  @Post()
  async createUser(@Body() dto: CreateUserDto) {
    await this.userService.validateNotDuplicateEmail(
      dto.email,
      dto.dni,
      dto.phone,
      dto.license,
      dto.userType === 'POLICE' ? true : false,
    );
    const userCreated = await this.userService.createUser(dto);

    return userCreated;
  }

  @Post('login')
  async login(@Body() dto: LoginDto) {
    const { token } = await this.userService.login(dto);

    return { token };
  }

  @Get(':id')
  async getOne(@Param('id') id: string) {
    const user = await this.userService.getOne(id);

    return user;
  }

  @Put(':id')
  async updateUser(@Param('id') id: string, @Body() dto: any) {

    const user = await this.userService.editUser(id, dto);

    return user;
  }

  @Delete(':id')
  async deleteUser(@Param('id') id: string) {
    const deleted = await this.userService.deleteUser(id);

    return deleted;
  }
}
