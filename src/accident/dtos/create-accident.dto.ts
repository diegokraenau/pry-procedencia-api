import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateAccidentDto {
  @IsString()
  @IsNotEmpty()
  latitude: string;

  @IsString()
  @IsNotEmpty()
  longitude: string;

  @IsString()
  @IsNotEmpty()
  dateCreated: string;

  @IsString()
  @IsNotEmpty()
  plate: string;

  @IsString()
  @IsNotEmpty()
  owner: string;


  @IsString()
  @IsNotEmpty()
  phone: string;

  @IsOptional()
  @IsString()
  description: string;

  @IsOptional()
  @IsString()
  conclusion: string;

  @IsOptional()
  @IsString()
  address: string;

  @IsString()
  user: string;

  @IsString()
  @IsOptional()
  userPolice: string;
  //commit

  @IsString()
  @IsOptional()
  soat: string;
}
