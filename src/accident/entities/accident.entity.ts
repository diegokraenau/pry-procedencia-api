import { User } from 'src/user/entities';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  JoinColumn,
  ManyToOne,
} from 'typeorm';

@Entity('accidents')
export class Accident {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'latitude', type: 'varchar' })
  latitude: string;

  @Column({ name: 'longitude', type: 'varchar' })
  longitude: string;

  @Column({ name: 'status', type: 'tinyint', default: 0 })
  status: number;

  @Column({ name: 'date_created', type: 'varchar' })
  dateCreated: string;

  @Column({ name: 'plate', type: 'varchar' })
  plate: string;

  @Column({ name: 'owner', type: 'varchar' })
  owner: string;

  @Column({ name: 'phone', type: 'varchar' })
  phone: string;

  @Column({ name: 'description', type: 'varchar', nullable: true })
  description: string;

  @Column({ name: 'conclusion', type: 'varchar', nullable: true })
  conclusion: string;

  @Column({ name: 'adress', type: 'varchar', nullable: true })
  address: string;

  @ManyToOne((type) => User, (user) => user.accidents)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @Column({ name: 'user_police', type: 'varchar', nullable: true })
  userPolice: string;
  
  @Column({ name: 'soat', type: 'varchar', nullable: true })
  soat: string;
}
