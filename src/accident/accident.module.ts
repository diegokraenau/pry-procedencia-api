import { forwardRef, Module } from '@nestjs/common';
import { AccidentController } from './accident.controller';
import { AccidentService } from './accident.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Accident } from './entities/accident.entity';
import { UserModule } from 'src/user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([Accident]), forwardRef(() => UserModule)],
  controllers: [AccidentController],
  providers: [AccidentService],
})
export class AccidentModule {}
