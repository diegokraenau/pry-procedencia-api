import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PoliceStation } from './entities/police-station.entity';
import { PoliceStationService } from './police-station.service';
import { PoliceStationController } from './police-station.controller';

@Module({
  imports: [TypeOrmModule.forFeature([PoliceStation])],
  providers: [PoliceStationService],
  exports: [PoliceStationService],
  controllers: [PoliceStationController],
})
export class PoliceStationModule {}
